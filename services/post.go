package services

import (
	"encoding/json"
	"github.com/Jeffail/gabs"
	"github.com/PuerkitoBio/goquery"
	"github.com/anaskhan96/soup"
	"io"
	"log"
	"net/http"
	"regexp"
	"strconv"
	"vnec/models"
	"vnec/utils"
)

type PostService struct {
	Post     *models.Post
	Database *Database
}

type IPostService interface {
	GetPosts(url string, params ...int) []models.Post
}

func (m PostService) GetPosts(url string, params ...int) []models.Post {
	var ps []models.Post

	targetPage := 1
	currentPage := 1
	if len(params) > 0 {
		targetPage = params[0]
	}
	if len(params) > 1 {
		currentPage = params[1]
	}

	newUrl := "https://vnexpress.net/" + url
	if targetPage > 0 {
		newUrl += "-p" + strconv.Itoa(currentPage)
	}

	req, _ := http.Get(newUrl)
	req.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36\n")

	body, _ := io.ReadAll(req.Body)

	doc := soup.HTMLParse(string(body))
	titles := doc.FindAll("", "class", "title-news")
	for _, title := range titles {
		postURL := title.Find("a").Attrs()["href"]

		var re = regexp.MustCompile(`(-)([0-9]+){1}(.html)`)
		matches := re.FindStringSubmatch(postURL)
		postID, _ := strconv.Atoi(matches[2])

		ps = append(ps, models.Post{
			ID:  postID,
			URL: postURL})
	}

	if len(ps) > 0 && targetPage > currentPage {
		return append(ps, m.GetPosts(url, targetPage, currentPage+1)...)
	}

	return ps
}

func (ps *PostService) GetPost() *models.Post {
	req, _ := http.Get(ps.Post.URL)
	req.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36")

	doc, err := goquery.NewDocumentFromReader(req.Body)
	if err != nil {
		log.Fatal(err)
	}

	ps.Post.Title = doc.Find("h1.title-detail").Text()

	doc.Find("p.description span.location-stamp").Remove()
	ps.Post.Description = doc.Find("p.description").Text()

	ps.Post.Thumbnail, _ = doc.Find("meta[itemprop=\"thumbnailUrl\"]").Attr("content")

	doc.Find("article.fck_detail").Children().Each(func(index int, selection *goquery.Selection) {
		className, classExist := selection.Attr("class")
		if !classExist {
			return
		}
		detail := models.PostDetail{}

		switch className {
		case "Normal":
			detail.Type = "text"
			detail.Value = selection.Text()
		case "tplCaption":
			detail.Type = "image"
			image := selection.Find("img[itemprop=\"contentUrl\"]")
			detail.Value, _ = image.Attr("data-src")
			detail.Description, _ = image.Attr("alt")
		}

		if detail.Type == "" {
			return
		}
		ps.Post.Detail = append(ps.Post.Detail, detail)
	})
	return ps.Post
}

func (ps *PostService) GetPostComments() *[]models.PostComment {
	// Get sign
	req, _ := http.Get(ps.Post.URL)
	req.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36")

	doc, err := goquery.NewDocumentFromReader(req.Body)
	if err != nil {
		log.Fatal(err)
	}

	sign, _ := doc.Find("div#box_comment_vne").Attr("data-component-input")

	type data struct {
		Sign         string
		Article_type string
	}
	var d data
	err = json.Unmarshal([]byte(sign), &d)

	//
	req, _ = http.Get("https://usi-saas.vnexpress.net/index/get?offset=0&limit=999999999&frommobile=0&sort=like&objectid=" + strconv.Itoa(ps.Post.ID) + "&objecttype=" + d.Article_type + "&siteid=1000000&sign=" + d.Sign)
	req.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36")
	req.Header.Set("Accept", "*/*")
	req.Header.Set("Accept-Encoding", "gzip, deflate, br")
	req.Header.Set("Connection", "keep-alive")

	doc, err = goquery.NewDocumentFromReader(req.Body)
	if err != nil {
		log.Fatal(err)
	}

	jsonParsed, err := gabs.ParseJSON([]byte(doc.Text()))
	if err != nil {
		panic(err)
	}
	a := jsonParsed.Search("data", "items")
	ac, _ := a.Children()

	for _, child := range ac {
		id := child.Path("comment_id").Data()
		name := child.Path("full_name").Data()
		content := child.Path("content").Data()
		like := child.Path("userlike").Data()
		date := child.Path("creation_time").Data()

		reply := child.Search("replys").Path("total").Data()

		if reply == nil {
			reply = 0
		} else {
			reply = int(reply.(float64))
		}

		comments := models.PostComment{
			ID:      id.(string),
			Name:    name.(string),
			Content: content.(string),
			Like:    int(like.(float64)),
			Date:    int64(date.(float64)),
			Reply: models.PostCommentReply{
				Count: reply.(int),
			},
		}

		if reply.(int) > 0 {
			comments.Reply.Item = *ps.getPostCommentReplys(&comments)
		}

		ps.Post.Comment = append(ps.Post.Comment, comments)
	}

	return &ps.Post.Comment
}

func (ps *PostService) getPostCommentReplys(postComment *models.PostComment) *[]models.PostCommentReplyItem {
	rs := []models.PostCommentReplyItem{}

	req, _ := http.Get("https://usi-saas.vnexpress.net/index/getreplay?offset=0&id=" + postComment.ID + "&limit=9999999")
	req.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36")
	req.Header.Set("Accept", "*/*")
	req.Header.Set("Accept-Encoding", "gzip, deflate, br")
	req.Header.Set("Connection", "keep-alive")

	doc, err := goquery.NewDocumentFromReader(req.Body)
	if err != nil {
		log.Fatal(err)
	}

	jsonParsed, err := gabs.ParseJSON([]byte(doc.Text()))
	if err != nil {
		panic(err)
	}
	a := jsonParsed.Search("data", "items")
	ac, _ := a.Children()

	for _, child := range ac {

		id, _ := strconv.Atoi(child.Path("comment_id").Data().(string))
		content := child.Path("content").Data()
		like := child.Path("userlike").Data()
		date := child.Path("creation_time").Data()

		rs = append(rs, models.PostCommentReplyItem{
			ID:      id,
			Content: content.(string),
			Like:    int(like.(float64)),
			Date:    int64(date.(float64)),
		})
	}

	return &rs
}

func (ps *PostService) SavePost() bool {
	detail, _ := utils.JSON_Marshal(ps.Post.Detail)
	comment, _ := utils.JSON_Marshal(ps.Post.Comment)

	if string(detail) == "null" {
		detail = nil
	}
	if string(comment) == "null" {
		comment = nil
	}

	site := models.PostTable{
		ID:          ps.Post.ID,
		URL:         ps.Post.URL,
		Title:       ps.Post.Title,
		Description: ps.Post.Description,
		Thumbnail:   ps.Post.Thumbnail,
		Detail:      string(detail),
		Comment:     string(comment),
	}

	var count int64
	result := ps.Database.connect.Model(&models.PostTable{}).Where("id = ?", ps.Post.ID).Count(&count)

	if count >= 1 {
		result = ps.Database.connect.Model(&site).Updates(&site)
	} else{
		result = ps.Database.connect.Create(&site)
	}

	if result.RowsAffected > 0 {
		return true
	}
	return false
}
