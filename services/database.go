package services

import (
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"vnec/configs"
	"vnec/models"
)

type Database struct {
	connect *gorm.DB
}

type IDatabase interface {
	Connect() *gorm.DB
}

var dbc *gorm.DB

func (db *Database) Connect() *gorm.DB {
	if dbc != nil {
		db.connect = dbc
		return db.connect
	}
	dbc, err := gorm.Open(mysql.Open(configs.DATABASE_CONNECT), &gorm.Config{
		Logger: logger.Default.LogMode(logger.Silent),
	})
	if err != nil {
		return nil
	}
	db.connect = dbc

	db.connect.AutoMigrate(&models.PostTable{})
	return dbc
}

func (db *Database) Create(value interface{}) *gorm.DB {
	return db.connect.Create(value)
}