module vnec

go 1.17

require (
	github.com/Jeffail/gabs v1.4.0
	github.com/PuerkitoBio/goquery v1.8.0
	github.com/anaskhan96/soup v1.2.5
	gorm.io/driver/mysql v1.3.5
	gorm.io/gorm v1.23.8
)

require (
	github.com/andybalholm/cascadia v1.3.1 // indirect
	github.com/go-sql-driver/mysql v1.6.0 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
	golang.org/x/net v0.0.0-20210916014120-12bc252f5db8 // indirect
	golang.org/x/text v0.3.6 // indirect
)
