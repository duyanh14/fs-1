package main

import (
	"fmt"
	"time"
	"vnec/services"
)

func main() {
	var db = services.Database{}
	db.Connect()

	var ps = services.PostService{}
	posts := ps.GetPosts("giao-duc/tin-tuc", 1)

	for _, post := range posts {
		post := post
		go func(){
			var ps = services.PostService{
				Post:     &post,
				Database: &db,
			}

			ps.GetPost()
			ps.GetPostComments()
			savePost := ps.SavePost()

			fmt.Println("ID: ", post.ID)
			fmt.Println("Title: ", post.Title)
			fmt.Println("Description: ", post.Description)
			fmt.Println("Save: ", savePost)
			fmt.Println("======================================")
		}()
	}

	for {
		time.Sleep(time.Second* 10)
	}
}
