package models

type Post struct {
	ID          int           `json:"id"`
	URL         string        `json:"url"`
	Title       string        `json:"title,omitempty"`
	Description string        `json:"description,omitempty"`
	Thumbnail   string        `json:"thumbnail,omitempty"`
	Detail      []PostDetail  `json:"detail,omitempty"`
	Comment     []PostComment `json:"comment,omitempty"`
}

type PostTable struct {
	ID          int `gorm:"primaryKey"`
	URL         string
	Title       string `sql:"type:CHARACTER SET utf8 COLLATE utf8_general_ci"`
	Description string `sql:"type:CHARACTER SET utf8 COLLATE utf8_general_ci"`
	Thumbnail   string
	Detail      string `sql:"type:CHARACTER SET utf8 COLLATE utf8_general_ci"`
	Comment     string `sql:"type:CHARACTER SET utf8 COLLATE utf8_general_ci"`
}
func (PostTable) TableName() string {
	return "posts"
}

type PostDetail struct {
	Type        string `json:"type"`
	Value       string `json:"value"`
	Description string `json:"description,omitempty"`
}

type PostComment struct {
	ID      string           `json:"id"`
	Name    string           `json:"name"`
	Content string           `json:"content"`
	Like    int              `json:"like"`
	Date    int64            `json:"date"`
	Reply   PostCommentReply `json:"reply"`
}

type PostCommentReply struct {
	Count int                    `json:"count"`
	Item  []PostCommentReplyItem `json:"item,omitempty"`
}

type PostCommentReplyItem struct {
	ID      int    `json:"id"`
	Content string `json:"content"`
	Like    int    `json:"like"`
	Date    int64  `json:"date"`
}